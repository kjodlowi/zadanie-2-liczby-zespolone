#pragma once
#include <iostream>

/*!
 * Modeluje pojecie liczby zespolonej
 */
struct Complex
{
    double re; /*! Pole repezentuje czesc rzeczywista. */
    double im; /*! Pole repezentuje czesc urojona. */
};

Complex operator+(Complex arg1, Complex arg2);

Complex operator-(Complex arg1, Complex arg2);

Complex operator*(Complex arg1, Complex arg2);

/*
* Realizuje dzielenie dwoch liczb zespolonych arg1 przez arg2
* Przed dzieleniem sprawdza, czy arg2 nie jest rowny zero.
*/
Complex operator/(Complex arg1, Complex arg2);

/*!
 * Realizuje dzielenie liczby zespolonej przez rzeczywista
 * Przed dzieleniem sprawdza, czy liczba t nie jest rowna 0.
 * Argumenty:
 *    arg1 - liczba zespolona
 *    t - liczba rzeczywista
 * Zwraca:
 *    Wynik dzielenia arg1 przez t
 */
Complex operator/(Complex arg1, double t); /* operator dzielenia liczby zespolonej przez rzeczywista */

/* Funkcja wyświetlająca liczbę zespoloną arg1 */
void CDisplay(Complex arg1);

/* przeciążenia operatora <<, wyświetlanie liczby zespolonej arg1 */
std::ostream& operator<< (std::ostream& StrmOut, Complex arg1); /* operator wyswietlania wyrazen */

/*
*  Funkcja porownujaca liczby zespolone arg1 i arg2
*  Argumenty: arg1, arg2 - liczby zespolone, ktore chcemy porownac
*/
int operator==(Complex arg1, Complex arg2);

/* Zwraca sprzezenie liczby zespolonej arg1 */
Complex conjugate(Complex arg1); 

/* funkcja wykonująca moduł liczby zespolonej arg1 */
double mod1(Complex arg1); 

/*!
 * Realizuje wczytywanie liczby zespolonej.
 * Argumenty:
 *    -plik - wskaźnik na plik, z którego czytamy liczby zespolone
 * Zwraca:
 *    Liczbe zespolona i informacje o bledzie jesli wystapil przy wczytywaniu liczby zespolonej.
 */
Complex Cread(FILE *plik);

/* Przeciazenie operatora >>, wczytywanie liczby zespolonej */
std::istream& operator>> (std::istream & StrmIn, Complex &arg1); 

double arg(Complex arg1);

Complex& operator-= (Complex & arg1, Complex &arg2);

Complex& operator*= (Complex & arg1, Complex &arg2);