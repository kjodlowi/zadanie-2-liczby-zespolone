#pragma once

#include "Complex.hh"

/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator
{
    kAddition,
    kSubtraction,
    kMultiplication,
    kDivision
};

/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct Expression
{
    Complex arg1; // Pierwszy argument wyrazenia arytmetycznego
    Operator op;  // Opertor wyrazenia arytmetycznego
    Complex arg2; // Drugi argument wyrazenia arytmetycznego
};

/*!
 * Realizuje wyswietlanie wyrazenia zespolonego
 * Argumenty:
 *    expr - wyrazenie zespolone
 * Zwraca:
 *    Na strumieniu standardowym wyswietla wyrazenie zespolone
 */
void Display(Expression expr); /* wyswietlanie wyrazenia zespolonego */

/*wczytywanie wyrazenia zespolonego */
std::ostream& operator<< (std::ostream & StrmOut, Expression expr); 

/*!
 * Rozwiązuje wyrażenie zespolone.
 * Parametry:
 *    expr - wyrażenie zespolone.
 * Zwraca:
 *    Wynik wyrażenia zespolonego.
 */
Complex Solve(Expression expr);

/* 
* Funckja wczytująca wyrażenie zespolone w postaci (a+bi)(operator)(c+di) .
* Argumenty:
*   -plik - wskaznik na plik, z ktorego czytamy 
* Zwraca:
*   Wyrazenie zespolone lub informacje o koncu pliku lub ewentualnym bledzie przy wczytywaniu
*/
Expression Eread(FILE *plik); /* wczytywanie wyrazenia zespolonego */
