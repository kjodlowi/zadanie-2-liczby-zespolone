#pragma once

struct Statistics {
    float max_score = 0;
    float score = 0;
};

/*!
 * Realizuje wyswietlanie statystyk
 * Parametry:
 *    stat - ogolna ilosc pytan i pytania na ktore uzytkownik odpowiedzial poprawnie
 * 
 * Wyswietla statystyki. 
 */
void Display( Statistics stat );

