#include <iostream>
#include <fstream>
#include <string>

#include "Expression.hh"
#include "Complex.hh"

using namespace std;

void Display(Expression expr)
{
    CDisplay(expr.arg1);
    switch (expr.op)
    {
    case kAddition:
        cout << "+";
        break;
    case kSubtraction:
        cout << "-";
        break;
    case kMultiplication:
        cout << "*";
        break;
    case kDivision:
        cout << "/";
        break;
    default:
        cout << "brak operatora" << endl;
    }
    CDisplay(expr.arg2);
}

ostream &operator<<(ostream &StrmOut, Expression expr)
{
    Display(expr);
}

Complex Solve(Expression expr)
{
    Complex result;

    switch (expr.op)
    {
    case kAddition:
        result = expr.arg1 + expr.arg2;
        break;

    case kSubtraction:
        result = expr.arg1 - expr.arg2;
        break;

    case kMultiplication:
        result = expr.arg1 * expr.arg2;
        break;

    case kDivision:
        result = expr.arg1 / expr.arg2;
        break;
    }

    return result;
}

Expression Eread(FILE *plik)
{
    Expression result;
    char c;
    try
    {
        c = fgetc(plik);
        if (c == EOF)
        {
            throw 2; /* przekazanie informacji o napotkaniu końca pliku */
        }else{ungetc(c,plik);}
        result.arg1 = Cread(plik);
        while ((c = fgetc(plik)) == ' '); /* usuwanie przerw pomiędzy liczbą zespoloną, a znakiem */

        switch (c)
        {
        case '+':
            result.op = kAddition;
            break;

        case '-':
            result.op = kSubtraction;
            break;

        case '*':
            result.op = kMultiplication;
            break;

        case '/':
            result.op = kDivision;
            break;
        default: throw 1; break; /* przekazanie informacji oblednie wczytanym wyrazeniu (zly znak) */
        }
        while ((c = fgetc(plik)) == ' '); /* usuwanie przerw pomiędzy znakiem, a liczbą zespoloną */
        ungetc(c,plik);
        result.arg2 = Cread(plik);

    }catch(const int i)
    {   
       throw 1; /* przekazanie informacji o blednie wczytanym wyrazeniu (blednie podana liczba zespolona) */
    }

    return result;
}