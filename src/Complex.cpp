#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <fstream>

#include "Complex.hh"
#include "Expression.hh"

using namespace std;

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik dodawania,
 *    arg2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */

Complex operator+(Complex arg1, Complex arg2)
{
  Complex result;

  result.re = arg1.re + arg2.re;
  result.im = arg1.im + arg2.im;
  return result;
}

/*!
 * Realizuje odejmowanie liczb zespolonych
 * Argumenty:
 *    arg1 - pierwszy skladnik odejmowania
 *    arg2 - drugi skladnik odejmowania
 * Zwraca:
 *     Roznice dwoch skladnikow.
 */
Complex operator-(Complex arg1, Complex arg2)
{
  Complex result;

  result.re = arg1.re - arg2.re;
  result.im = arg1.im - arg2.im;
  return result;
}

Complex operator*(Complex arg1, Complex arg2)
{
  Complex result;

  result.re = arg1.re * arg2.re - arg1.im * arg2.im;
  result.im = arg1.re * arg2.im + arg2.im * arg2.re;
  return result;
}

/*!
 * Realizuje dzielenie dwoch skladnikow.
 * Przed dzieleniem sprawdza, czy arg2 nie jest rowny zero.
 */
Complex operator/(Complex arg1, Complex arg2)
{
  if (arg2.re == 0 && arg2.im == 0)
  {
    cout << "Division by 0" << endl;
  }
  else
  {
    Complex result;

    result = (arg1*conjugate(arg2))/pow(2,mod1(arg2));
  }
}

Complex operator/(Complex arg1, double t)
{
  Complex result;
  if(t != 0)
  {
    result.re = (arg1.re/t);
    result.im = (arg1.im/t);
  }
  else
  {
    cout << "Division by 0 " << endl;
  }
  return result;
}

void CDisplay(Complex arg1) 
{

  if (arg1.im > 0.00)
  {
    cout << setprecision(2) << fixed << "(" << arg1.re << "+" << arg1.im << "i)";
  }
  if (arg1.im < 0.00)
  {
    cout << setprecision(2) << fixed << "(" << arg1.re << "-" << (arg1.im * (-1)) << "i)";
  }
  if (arg1.im == 0.00)
  {
    cout << setprecision(2) << fixed << "(" << arg1.re << "+0.00i)";
  }
} 

ostream& operator<< (ostream & StrmOut, Complex arg1)
{
  CDisplay(arg1);
}

int operator==(Complex arg1, Complex arg2)
{
  if (arg1.re == arg2.re)
  {
    if (arg1.im == arg2.im)
    {
      return 1;
    }
  }
  return 0;
}

Complex conjugate(Complex arg1) 
{
  Complex result;

  result.re = arg1.re;
  if (arg1.im != 0)
  {
    result.im = (-1) * arg1.im;
  }
  else
  {
    result.im = 0;
  }

  return result;
}

double mod1(Complex arg1) 
{
  double result;

  result = sqrt(pow(arg1.re, 2) + pow(arg1.im, 2));

  return result;
}

Complex Cread(FILE *plik){

  Complex result;
  char c = 'e';
  double num1=0,num2=0;

  if((fscanf(plik,"(%lf%c%lfi)",&num1,&c,&num2)) == 3){
    
    if(c=='-'){
      num2 = num2 * (-1);
    }
    result.re = num1;
    result.im = num2;

  }else{
    while (fgetc(plik) != 10);
    throw 1;
  }

  return result;
}

istream& operator>> (istream & StrmIn, Complex &arg1) 
{
 Complex result;
  char c;
  double num1=0,num2=0;

  if(scanf("(%lf%c%lfi)",&num1,&c,&num2) == 3){
    if(c=='-'){
      num2 = num2 * (-1);
    }
    result.re = num1;
    result.im = num2;
    arg1 = result;
  }else{
    while (fgetc(stdin) != 10);
    throw 1;
  }
  
  return StrmIn;
}

double arg(Complex arg1)
{
  double result;
  if (arg1.re != 0)
  {
    result = atan2(arg1.im,arg1.re);
  }else
  {
    if (arg1.im != 0)
    {
      if (arg1.im > 0)
      {
        result = (1/2)*3.1415;
      }else
      {
        result = -(1/2)*3.1415;
      }
      return result;
    }else
    {
      cerr << "Argument jest nieokreslony :/" << endl;
    }
  }
  
}

Complex& operator-= (Complex & arg1, Complex &arg2) 
{
  
  arg1.re = arg1.re - arg2.re;
  arg1.im = arg1.im - arg2.im;
  
  return arg1;
}
Complex& operator*= (Complex & arg1, Complex &arg2) 
{
  
  arg1.re = arg1.re * arg2.re - arg1.im * arg2.im;
  arg1.im = arg1.re * arg2.im + arg2.im * arg2.re;
  
  return arg1;
}