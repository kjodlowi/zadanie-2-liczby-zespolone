#include <iostream>
#include "Complex.hh"
#include "Database.hh"
#include "Expression.hh"
#include "Statistics.hh"

using namespace std;

int main(int argc, char **argv)
{

    if (argc < 2)
    {
        cout << endl;
        cout << " Brak opcji okreslajacej rodzaj testu." << endl;
        cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
        cout << endl;
        return 1;
    }

    Database base = {nullptr, 0, 0};

    if (Init(&base, argv[1]) == false)
    {
        cerr << " Inicjalizacja testu nie powiodla sie." << endl;
        return 1;
    }

    cout << endl;
    cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
    cout << endl;

    Expression expression;

    Complex a;
    int err = 0;
    int *ptr = &err;
    int i = 0;
    Statistics stats;

    while (GetNextQuestion(&base, &expression))
    {
        stats.max_score++;
        do
        {
            cout << "Podaj wynik operacji: " << expression << endl;
            try
            {
                cin >> a;
                {
                    if (Solve(expression) == a)
                    {
                        stats.score++;
                        cout << endl
                             << "Dobra odpowiedz" << endl
                             << endl;
                        while ((getchar()) != '\n')
                            ;
                        i = 5;
                    }
                    else
                    {
                        cout << endl
                             << "Zla odpowiedz" << endl
                             << endl;
                        while ((getchar()) != '\n')
                            ;
                        i = 5;
                    }
                }
            }
            catch (const int n)
            {
                i++;
                if (i == 3)
                {
                    cout << endl
                         << "Zla odpowiedz!"
                         << endl;
                }
                else
                {
                    cout << endl
                         << "Blednie wprowadzona liczba zespolona, pozostale szanse: " << (3 - i) << endl;
                }
            }

        } while (i < 3);

        i = 0;
    }
    Display(stats);
    cout << endl;
    cout << " Koniec testu" << endl;
    cout << endl;
}
