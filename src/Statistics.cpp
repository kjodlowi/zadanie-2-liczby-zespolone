#include <iostream>
#include <iomanip>

#include "Statistics.hh"

using namespace std;

void Display( Statistics  stat )
{
  cout << "Statystyka Testu: " << endl;
  cout << "Ilosc poprawnych odpowiedzi: " << stat.score << endl;
  cout << "Ilosc blednych odpowiedzi: " << stat.max_score - stat.score << endl;
  cout << "Wynik procentowy: " << 100 * stat.score/stat.max_score << "%" << endl; 
}
